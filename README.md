# Project HRnet

HRnet is an application react for create and list employee in company.

## Dependencies
   - yarn or npm,
   - react-router-dom,
   - [react-datepicker](https://reactdatepicker.com/),
   - [tailwindcss](https://tailwindcss.com/) | craco.
   - [mllt-datatable](https://www.npmjs.com/package/mllt-datatable/v/1.2.2)

## Installation

I use the package manager yarn for this project.

Step 1 : Clone this project

```bash
git clone https://gitlab.com/mlltmax66/maximemoilliet_15_14092021.git
``` 

Step 2 : Go to the project folder

```bash
cd maximemoilliet_15_14092021
```

Step 3 : Install all dependencies

```bash
yarn install
```

Step 4 : Run app

```bash
yarn start
```

Start the tests :

```bash
yarn test
```