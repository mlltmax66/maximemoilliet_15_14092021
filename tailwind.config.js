module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      white: "#ffffff",
      dark : "#111827",
      red: "#f56565",
      'gray-700': "#e2e8f0",
      'gray-900': "#96a1b2",
      'green-500': "#93ad18",
      'green-700': "#5a6f08",
      'blue-600': "#2563EB",
    },
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [], 
}
