import React, { useState } from 'react'
import PropTypes from 'prop-types'
import Header from '../components/bases/Header'
import CreateEmployeeForm from '../components/createEmployeeForm/CreateEmployeeForm'
import { useHistory } from 'react-router-dom';
import Modal from '../components/ui/Modal';


export default function CreateEmployee({ createEmployee }) {
    const history = useHistory()
    const [isSubmitting, setIsSubmitting] = useState(false);

    function submitForm() {
        setIsSubmitting(true)
    }

    const handleClose = () => {
        history.push('/')
    }

    return (
        <>
            <Header path="/" link="View Current Employees" text="Create Employee" />
            <main>
                <CreateEmployeeForm submitForm={submitForm} createEmployee={createEmployee} />
                {isSubmitting && <Modal message="Employee created!" onClose={handleClose} />}
            </main>
        </>
    )
}

CreateEmployee.propTypes = {
    createEmployee: PropTypes.func.isRequired
}



