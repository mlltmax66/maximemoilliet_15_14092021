import React from 'react'
import PropTypes from 'prop-types'
import Header from '../components/bases/Header'
import { employeesLabels } from '../data/employeesData'
import DataTable from 'mllt-datatable'
import 'mllt-datatable/dist/index.css'

export default function ListEmployee({ employees }) {
    return (
        <>
            <Header path="/create-employee" link="Home" text="Current Employees" />
            {employees && <div className="container mx-auto mt-5"><DataTable data={employees} labels={employeesLabels} itemsPerPage={10} /></div>}
        </>
    )
}


ListEmployee.propTypes = {
    employees: PropTypes.array,
}
