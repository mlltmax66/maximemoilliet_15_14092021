import React, { useState } from 'react'
import { Switch, Route } from 'react-router-dom'
import { employeesData } from '../data/employeesData'
import CreateEmployee from '../screens/CreateEmployee'
import ListEmployee from '../screens/ListEmployee'

export default function Routes() {
    const [employees, setEmployees] = useState(employeesData)

    const createEmployee = (values) => {
        setEmployees([...employees, values])
    }

    return (
        <Switch>
            <Route exact path="/"><ListEmployee employees={employees} /></Route>
            <Route exact path="/create-employee"><CreateEmployee createEmployee={createEmployee} /></Route>
        </Switch>
    )
}
