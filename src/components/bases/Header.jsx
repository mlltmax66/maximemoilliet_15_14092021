import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'

const Header = ({ path, link, text }) => {
    return (
        <header className="py-5 flex flex-col justify-center items-center w-full gap-4">
            <h1 className="text-5xl font-semibold text-green-500">HRnet</h1>
            <Link to={path} className="text-green-700 text-lg underline">{link}</Link>
            <p className="text-xl text-dark font-semibold">{text}</p>
        </header>
    )
}

Header.propTypes = {
    path: PropTypes.string.isRequired,
    link: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired
}

export default Header

