import React from 'react'
import PropTypes from 'prop-types'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import useCreateEmployeeForm from './useCreateEmployeeForm'
import { departmentsSelect } from '../../data/departments'
import Dropdown from '../ui/Dropdown'
import { statesSelect } from '../../data/states'

const CreateEmployeeForm = ({ submitForm, createEmployee }) => {
    const { values, handleChangeCustom, handleChange, handleSubmit, errors } = useCreateEmployeeForm(submitForm, createEmployee)

    return (
        <div className="mx-auto w-full max-w-md mt-10 mb-40">
            <form onSubmit={handleSubmit} className="bg-white shadow-lg rounded px-8 pt-8 pb-8 mb-4">
                <div className="mb-4">
                    <label className="block text-dark text-lg font-bold mb-2" htmlFor="firstname">First name</label>
                    <input 
                        className={!errors.firstName 
                            ? "shadow appearance-none border rounded text-lg w-full py-2 px-3 text-dark leading-tight border-gray-900"
                            : "shadow appearance-none border rounded text-lg w-full py-2 px-3 text-dark leading-tight border-red"
                        } 
                        type="text" 
                        id="firstname"
                        name="firstName" 
                        placeholder="Elon"
                        value={values.firstName} 
                        onChange={handleChange} />
                    {errors.firstName && <p className="text-red text-sm italic">{errors.firstName}</p>}
                </div>
                <div className="mb-4">
                    <label className="block text-dark text-lg font-bold mb-2" htmlFor="lastName">Last name</label>
                    <input 
                        className={!errors.lastName 
                            ? "shadow appearance-none border text-lg rounded w-full py-2 px-3 text-dark leading-tight border-gray-900"
                            : "shadow appearance-none border text-lg rounded w-full py-2 px-3 text-dark leading-tight border-red"
                        } 
                        type="text" 
                        id="lastName"
                        name="lastName" 
                        placeholder="Musk"
                        value={values.lastName} 
                        onChange={handleChange} />
                    {errors.lastName && <p className="text-red text-sm italic">{errors.lastName}</p>}
                </div>
                <div className="mb-4">
                    <label className="block text-dark text-lg font-bold mb-2" htmlFor="dateBirth">Date of birth</label>
                    <DatePicker 
                        id="dateBirth" 
                        className={errors.dateBirth 
                            ? "shadow appearance-none border text-lg rounded w-full py-2 px-3 text-dark leading-tight border-red"
                            : "shadow appearance-none border text-lg rounded w-full py-2 px-3 text-dark leading-tight border-gray-900" 
                        } 
                        selected={values.dateBirth} 
                        onChange={date => handleChangeCustom('dateBirth', date)} 
                        dateFormat="dd/MM/yyyy" 
                        name="dateBirth" />
                    {errors.dateBirth && <p className="text-red text-md italic">{errors.dateBirth}</p>}
                </div>
                <div className="mb-4">
                    <label className="block text-dark text-lg font-bold mb-2" htmlFor="dateStart">Start date</label>
                    <DatePicker 
                        id="dateStart" 
                        className={errors.dateStart 
                            ? "shadow appearance-none border text-lg rounded w-full py-2 px-3 text-dark leading-tight border-red"
                            : "shadow appearance-none border text-lg rounded w-full py-2 px-3 text-dark leading-tight border-gray-900" 
                        } 
                        selected={values.dateStart} 
                        onChange={date => handleChangeCustom('dateStart', date)} 
                        dateFormat="dd/MM/yyyy" 
                        name="dateStart" />
                    {errors.dateStart && <p className="text-red text-md italic">{errors.dateStart}</p>}
                </div>
                <fieldset className="border border-gray-900 px-4 pt-3 rounded">
                    <legend className="text-green-700 text-lg font-bold">Address</legend>
                    <div className="mb-4">
                        <label className="block text-dark text-lg font-bold mb-2" htmlFor="street">Street</label>
                        <input 
                            className={!errors.street 
                                ? "shadow appearance-none border text-lg rounded w-full py-2 px-3 text-dark leading-tight border-gray-900"
                                : "shadow appearance-none border text-lg rounded w-full py-2 px-3 text-dark leading-tight border-red"
                            } 
                            type="text" 
                            id="street"
                            name="street" 
                            placeholder="16 New York Plaza"
                            value={values.street} 
                            onChange={handleChange} />
                        {errors.street && <p className="text-red text-sm italic">{errors.street}</p>}
                    </div>
                    <div className="mb-4">
                        <label className="block text-dark text-lg font-bold mb-2" htmlFor="city">City</label>
                        <input 
                            className={!errors.city 
                                ? "shadow appearance-none border rounded text-lg w-full py-2 px-3 text-dark leading-tight border-gray-900"
                                : "shadow appearance-none border rounded text-lg w-full py-2 px-3 text-dark leading-tight border-red"
                            }
                            type="text" 
                            id="city"
                            name="city" 
                            placeholder="New York"
                            value={values.city} 
                            onChange={handleChange} />
                        {errors.city && <p className="text-red text-sm italic">{errors.city}</p>}
                    </div>
                    <div className="mt-4">
                        <label className="block text-dark text-lg font-bold mb-2" htmlFor="state">State</label>
                        <Dropdown id="state" options={statesSelect} selected={values.state} setSelected={value => handleChangeCustom('state', value)} />
                    </div>
                    <div className="mt-4 mb-4">
                        <label className="block text-dark text-lg font-bold mb-2" htmlFor="zipCode">Zip code</label>
                        <input 
                            className={!errors.zipCode 
                                ? "shadow appearance-none border rounded text-lg w-full py-2 px-3 text-dark leading-tight border-gray-900"
                                : "shadow appearance-none border rounded text-lg w-full py-2 px-3 text-dark leading-tight border-red"
                            }
                            type="text" 
                            id="zipCode"
                            name="zipCode" 
                            placeholder="10007" 
                            value={values.zipCode} 
                            onChange={handleChange} />
                        {errors.zipCode && <p className="text-red text-sm italic">{errors.zipCode}</p>}
                    </div>
                </fieldset>
                <div className="mt-4">
                    <label className="block text-dark text-lg font-bold mb-2" htmlFor="department">Department</label>
                    <Dropdown id="department" options={departmentsSelect} selected={values.department} setSelected={value => handleChangeCustom('department', value)} />
                </div>
                <button className="mt-6 w-full text-lg bg-green-700 hover:bg-green-500 text-white font-bold py-2 px-4 rounded" type="submit">Save</button>
            </form>
        </div>
    )
}


CreateEmployeeForm.propTypes = {
    submitForm: PropTypes.func.isRequired,
    createEmployee: PropTypes.func.isRequired
}


export default CreateEmployeeForm
