import { useState, useEffect } from 'react'
import { departmentsSelect } from '../../data/departments'
import { statesSelect } from '../../data/states'
import validate from './createEmployeeFormValidation'

const useCreateEmployeeForm = (callback, createEmployee) => {
    const [isSubmitting, setIsSubmitting ] = useState(false)
    const [values, setValues] = useState({
        firstName: '',
        lastName: '',
        dateBirth: new Date(),
        dateStart: new Date(),
        street: '',
        city: '',
        state: statesSelect[0],
        zipCode: '',
        department: departmentsSelect[0]
    })

    const [errors, setErrors] = useState({})

    const handleChange = e => {
        const { name, value } = e.target
        setValues({
            ...values,
            [name]: value
        })
    }

    const handleChangeCustom = (name, value) => {
        setValues({
            ...values,
            [name]: value,
          });
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        setErrors(validate(values))
        setIsSubmitting(true)
    }
    
    useEffect(
        () => {
            if (Object.keys(errors).length === 0 && isSubmitting) {
              createEmployee(values)
              callback()
          }
        },
        // eslint-disable-next-line
        [errors, isSubmitting]
    )

    return { values, handleChange, handleChangeCustom, handleSubmit, errors }
}

export default useCreateEmployeeForm