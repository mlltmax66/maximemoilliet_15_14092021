const validate = (values) => {
    let errors = {}

    if(!values.firstName.trim()) {
        errors.firstName = 'First name is required'
    } else if(values.firstName.length < 3) {
        errors.firstName = 'First name needs to be 3 characters or more'
    }

    if(!values.lastName.trim()) {
        errors.lastName = 'Last name is required'
    } else if(values.lastName.length < 3) {
        errors.lastName = 'Last name needs to be 3 characters or more'
    }

    if(!values.dateBirth) {
        errors.dateBirth = 'Date of birth is required'
    }

    if(!values.dateStart) {
        errors.dateStart = 'Start date is required'
    }

    if(!values.street) {
        errors.street = 'Street is required'
    }

    if(!values.city.trim()) {
        errors.city = 'City is required'
    }

    if(!values.zipCode.trim()) {
        errors.zipCode = 'Zip code is required'
    } else if(!/^[0-9]+$/i.test(values.zipCode)) {
        errors.zipCode = 'Zip code is an number'
    }

    return errors
}

export default validate