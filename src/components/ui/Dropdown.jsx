import { useState } from "react"
import PropTypes from 'prop-types'

const Dropdown = ({ selected, setSelected, options }) => {
    const [isActive, setIsActive] = useState(false)

    return (
        <div className="shadow appearance-none border max-w-max rounded py-2 px-3 text-dark leading-tight border-gray-900">
            <div data-testid="dropdown-header" className="flex justify-between cursor-pointer" onClick={(e) => setIsActive(!isActive)}>
                <span className="text-lg">{selected}</span>
                {isActive ? <span className="fas fa-caret-up ml-10 text-lg"></span> : <span className="fas fa-caret-down ml-10 text-lg"></span>}
            </div>
            {isActive && (
                <ul className="absolute max-h-40 overflow-y-auto mt-4 -ml-3 max-w-max rounded shadow appearance-none border w-full py-2 px-3 text-dark leading-tight border-gray-900 bg-white">
                {options.map((option) => (
                    <li
                    key={option}
                    onClick={(e) => {
                        setSelected(option);
                        setIsActive(false);
                    }}
                    className="py-1 text-lg cursor-pointer hover:text-green-700"
                    >
                    {option}
                    </li>
                ))}
                </ul>
            )}
        </div>
    );
}

Dropdown.propTypes = {
    selected: PropTypes.string.isRequired,
    setSelected: PropTypes.func.isRequired,
    options: PropTypes.array.isRequired,
}

export default Dropdown
