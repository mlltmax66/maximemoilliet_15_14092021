import React, { useState } from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'

const Modal = ({ message, onClose, open = true }) => {
    const [openModal, setOpenModal] = useState(open)

    const handleClose = () => {
        setOpenModal(false)
        if(onClose) {
            onClose()
        }
    }

    return (
        ReactDOM.createPortal(
            openModal && <div className="fixed flex justify-center z-1 top-0 bottom-0 right-0 left-0 bg-dark bg-opacity-10">
                <div className="fixed shadow-xl bg-white rounded py-8 top-40 px-12 w-4xl">
                    <p className="text-grey-900 text-lg font-bold mt-6 mb-4">{message}</p>
                    <i data-testid="close-modal" onClick={handleClose} className="fas fa-times modal__icon hover:text-green-700 p-2 cursor-pointer text-xl absolute top-1 right-2"></i>
                </div>
            </div>,
            document.body
        )
    )
}

Modal.propTypes = {
    message: PropTypes.string.isRequired,
    onClose: PropTypes.func,
    open: PropTypes.bool
}

export default Modal

