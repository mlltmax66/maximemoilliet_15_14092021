import { cleanup, fireEvent, render, screen } from '@testing-library/react'
import Header from '../../../components/bases/Header';
import { createMemoryHistory } from 'history'
import { Router } from 'react-router-dom'

function renderWithRouter (
    ui,
    {
      route = '/',
      history = createMemoryHistory({ initialEntries: [route] })
    } = {}
  ) {
    return {
      ...render(<Router history={history}>{ui}</Router>),
      history
    }
}

describe('Header component', () => {

    afterEach(cleanup)

    test('Should render text and link props', () => {
        renderWithRouter(<Header path="/" link="View Current Employees" text="Create Employee" />)
        const link = screen.getByText(/View Current Employees/i)
        const text = screen.getByText(/Create Employee/i)
        expect(link).toBeInTheDocument()
        expect(text).toBeInTheDocument()
    }); 

    test('When click on link redirect user', () => {
        const { history } = renderWithRouter(<Header path="/create" link="View Current Employees" text="Create Employee" />)
        const link = screen.getByText(/View Current Employees/i)
        fireEvent.click(link)
        expect(history.location.pathname).toBe("/create")
    });
})