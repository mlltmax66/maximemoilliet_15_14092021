import { fireEvent, render, screen } from '@testing-library/react'
import Modal from '../../../components/ui/Modal';

describe('Modal component', () => {
    test('default modal is open', () => {
        render(<Modal message="Employee created" />)
        expect(screen.getByText(/Employee created/i)).toBeInTheDocument()
    });

    test('modal is close by default if open props equal false', () => {
        render(<Modal message="Employee created" open={false} />)
        expect(() => screen.getByText(/Employee created/i)).toThrow('Unable to find an element');
    });

    test('render message modal', () => {
        render(<Modal message="Employee created" />)
        expect(screen.getByText(/Employee created/i)).toBeInTheDocument()
    });
    
    test('when click on close button, modal is close', () => {
        render(<Modal message="Employee created" />)
        const buttonClose = screen.getByTestId('close-modal')
        fireEvent.click(buttonClose)
        expect(() => screen.getByText(/Employee created/i)).toThrow('Unable to find an element');
    });

    test('when click on close button and have onClose function in props, modal is close and function is called', () => {
        const handleClose = jest.fn()
        render(<Modal message="Employee created" onClose={handleClose} />)
        const buttonClose = screen.getByTestId('close-modal')
        fireEvent.click(buttonClose)
        expect(() => screen.getByText(/Employee created/i)).toThrow('Unable to find an element');
        expect(handleClose).toHaveBeenCalled()
    });
})
