import { cleanup, fireEvent, render, screen } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import { statesSelect } from '../../../data/states';
import { departmentsSelect } from '../../../data/departments';
import Dropdown from '../../../components/ui/Dropdown';

describe('Dropdown component', () => {

    afterEach(cleanup)

    test('When user click on dropdown render options ', () => {
        render(<Dropdown options={statesSelect} selected={statesSelect[0]} setSelected={() => null} />)
        const headerDropdown = screen.getByTestId('dropdown-header')
        expect(() => screen.getByText(/California/i)).toThrow('Unable to find an element');
        expect(() => screen.getByText(/Colorado/i)).toThrow('Unable to find an element');
        expect(() => screen.getByText(/Hawaii/i)).toThrow('Unable to find an element');
        fireEvent.click(headerDropdown)
        expect(screen.getByText(/California/i)).toBeInTheDocument()
        expect(screen.getByText(/Colorado/i)).toBeInTheDocument()
        expect(screen.getByText(/Hawaii/i)).toBeInTheDocument()
        fireEvent.click(headerDropdown)
        expect(() => screen.getByText(/California/i)).toThrow('Unable to find an element');
        expect(() => screen.getByText(/Colorado/i)).toThrow('Unable to find an element');
        expect(() => screen.getByText(/Hawaii/i)).toThrow('Unable to find an element');
    });

    test('When user click on option dropdown no render options and setSelected function to been called', () => {
        const mockSelected = jest.fn()
        render(<Dropdown options={departmentsSelect} selected={departmentsSelect[0]} setSelected={mockSelected} />)
        const headerDropdown = screen.getByTestId('dropdown-header')
        fireEvent.click(headerDropdown)
        const option = screen.getByText(/Engineering/i)
        fireEvent.click(option)
        expect(mockSelected).toHaveBeenCalledWith('Engineering')
    });
    
})
